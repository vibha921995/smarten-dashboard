import 'package:flutter/widgets.dart';

class MenuModel {
  String name;
  bool isSelected;
  bool isActive;
  IconData iconData;

  MenuModel(this.name, /*this.image,*/ this.iconData, this.isActive, this.isSelected);

}