// To parse this JSON data, do
//
//     final logResponse = logResponseFromJson(jsonString);

import 'dart:convert';

LogResponse logResponseFromJson(String str) => LogResponse.fromJson(json.decode(str));

String logResponseToJson(LogResponse data) => json.encode(data.toJson());

class LogResponse {
  LogResponse({
    this.success,
    this.response,
    this.message,
    this.token,
  });

  String success;
  List<LogModel> response;
  String message;
  String token;

  factory LogResponse.fromJson(Map<String, dynamic> json) => LogResponse(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : List<LogModel>.from(json["response"].map((x) => LogModel.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    token: json["token"] == null ? null : json["token"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : List<dynamic>.from(response.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "token": token == null ? null : token,
  };
}

class LogModel {
  LogModel({
    this.extractionDate,
    this.extractionStartTime,
    this.extractionEndTime,
    this.extractionStatus,
    this.totalCompanies,
  });

  DateTime extractionDate;
  DateTime extractionStartTime;
  DateTime extractionEndTime;
  String extractionStatus;
  String totalCompanies;

  factory LogModel.fromJson(Map<String, dynamic> json) => LogModel(
    extractionDate: json["extraction_date"] == null ? null : DateTime.parse(json["extraction_date"]),
    extractionStartTime: json["extraction_start_time"] == null ? null : DateTime.parse(json["extraction_start_time"]),
    extractionEndTime: json["extraction_end_time"] == null ? null : DateTime.parse(json["extraction_end_time"]),
    extractionStatus: json["extraction_status"] == null ? null : json["extraction_status"],
    totalCompanies: json["total_companies"] == null ? null : json["total_companies"],
  );

  Map<String, dynamic> toJson() => {
    "extraction_date": extractionDate == null ? null : extractionDate.toIso8601String(),
    "extraction_start_time": extractionStartTime == null ? null : extractionStartTime.toIso8601String(),
    "extraction_end_time": extractionEndTime == null ? null : extractionEndTime.toIso8601String(),
    "extraction_status": extractionStatus == null ? null : extractionStatus,
    "total_companies": totalCompanies == null ? null : totalCompanies,
  };
}
