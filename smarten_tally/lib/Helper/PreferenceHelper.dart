import 'dart:convert';
import 'package:smarten_tally/Models/LoginResponse.dart';
import 'package:smarten_tally/Utils/PrefUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smarten_tally/Models/RegisterResponse.dart';

class PreferenceHelper {
  SharedPreferences prefs;

  PreferenceHelper(SharedPreferences prefs) {
    this.prefs = prefs;
  }

  void saveIsUserNew(bool isLoggedIn) {
    prefs.setBool(PrefUtils.IS_USERNEW, isLoggedIn);
  }

  bool getIsUserNew() {
    return prefs.getBool(PrefUtils.IS_USERNEW) ?? true;
  }

  UserModel getUserData() {
    final parsed = json.decode(prefs.getString(PrefUtils.USER_DATA) ?? "{}");
    return UserModel.fromJson(parsed);
  }

  //
  void saveUserData(UserModel userData) {
    prefs.setString(PrefUtils.USER_DATA, json.encode(userData));
  }

  Response getUserDataRegister() {
    final parsed =
        json.decode(prefs.getString(PrefUtils.USER_DATA_REGISTER) ?? "{}");
    return Response.fromJson(parsed);
  }

  void saveCompanyData(String userData) {
    prefs.setString(PrefUtils.SEL_COMPANY,userData);
  }

  String getCompanyData() {
    // List<String> tempstr = [];
    // String temp = prefs.getString(PrefUtils.SEL_COMPANY);
    // if(temp!=null){
    //   tempstr = List.of(<String>{temp});
    // }
    return prefs.getString(PrefUtils.SEL_COMPANY)??"";
  }

  //
  void saveUserDataRegister(Response userData) {
    prefs.setString(PrefUtils.USER_DATA_REGISTER, json.encode(userData));
  }

  //
  // LoginResponse getUserDataArray() {
  //   final parsed = json.decode(prefs.getString(PrefUtils.USER_DATA_ARRAY) ?? "{}");
  //   return LoginResponse.fromJson(parsed);
  // }
  //
  // void saveUserDataArray(LoginResponse userData) {
  //   prefs.setString(PrefUtils.USER_DATA_ARRAY, json.encode(userData));
  // }

  void saveIsUserLoggedIn(bool isLoggedIn) {
    prefs.setBool(PrefUtils.IS_LOGGED_IN, isLoggedIn);
  }

  bool getIsUserLoggedIn() {
    return prefs.getBool(PrefUtils.IS_LOGGED_IN) ?? false;
  }

  void saveIsBalanceUpdated(bool isLoggedIn) {
    prefs.setBool(PrefUtils.IS_BALANCE_UPDATED, isLoggedIn);
  }

  bool getIsBalanceUpdated() {
    return prefs.getBool(PrefUtils.IS_BALANCE_UPDATED) ?? true;
  }

  void saveFirebaseToken(String token) {
    prefs.setString(PrefUtils.FB_TOKEN, token);
  }

  String getFirebaseToken() {
    return prefs.getString(PrefUtils.FB_TOKEN) ?? "";
  }

  // String getUname() {
  //   return prefs.getString(ApiUtils.USERNAME) ?? "";
  // }
  //
  // void setUname(String uname) {
  //   prefs.setString(ApiUtils.USERNAME, uname);
  // }
  //
  // String getPass() {
  //   return prefs.getString(ApiUtils.PASSWORD) ?? "";
  // }
  //
  // void setPass(String pass) {
  //   prefs.setString(ApiUtils.PASSWORD, pass);
  // }

  void saveLoginType(String type) {
    prefs.setString(PrefUtils.LOGIN_TYPE, type);
  }

  String getLoginType() {
    return prefs.getString(PrefUtils.LOGIN_TYPE) ?? "";
  }

  void saveReceiveAddress(String token) {
    prefs.setString(PrefUtils.RECEIVE_ADDRESS, token);
  }

  String getReceiveAddress() {
    return prefs.getString(PrefUtils.RECEIVE_ADDRESS) ?? "";
  }

  void saveUserBalance(String balance) {
    prefs.setString(PrefUtils.USER_BALANCE, balance);
  }

  String getUserBalance() {
    return prefs.getString(PrefUtils.USER_BALANCE) ?? "0.0";
  }

  void saveAppLanguage(String balance) {
    prefs.setString(PrefUtils.APP_LANG, balance);
  }

  String getAppLanguage() {
    return prefs.getString(PrefUtils.APP_LANG) ?? "en";
  }

  void clearAll() {
    prefs.remove(PrefUtils.USER_DATA);
    prefs.remove(PrefUtils.IS_LOGGED_IN);
    prefs.remove(PrefUtils.LOGIN_TYPE);
    prefs.remove(PrefUtils.SEL_COMPANY);
  }
}
