import 'dart:convert';
import 'dart:io';

import 'package:smarten_tally/Contants/AppColors.dart';
import 'package:smarten_tally/Contants/ConstantString.dart';
import 'package:smarten_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:smarten_tally/Helper/PreferenceHelper.dart';
import 'package:smarten_tally/Models/ErrorResponse.dart';
import 'package:smarten_tally/Models/LoginResponse.dart';
import 'package:smarten_tally/Utils/ApiUtils.dart';
import 'package:smarten_tally/Utils/screen_util.dart';
import 'package:smarten_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:http/http.dart' as http;
// import 'package:intl/intl.dart';
// import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/gestures.dart';
import 'package:smarten_tally/Views/DashBoardView.dart';
import 'CustomWebView1.dart';

class DashboardView1 extends StatefulWidget {

  final String strDashboardTitle;

  DashboardView1({Key key, @required this.strDashboardTitle}) : super(key: key);

  Widget currentView;
  bool isWebLoading = false;

  @override
  DashboardView1State createState() => DashboardView1State();
}

class DashboardView1State extends State<DashboardView1> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  bool isDesktopVerification = false;

  String webUrl = "";

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
  }

  Future<void> getSharedPreferenceObject() async {
    setState(() {
      // widget.isWebLoading = true;
      webUrl = "http://finance.smartenapps.com/mobile-app/smarten-angular.php";
      widget.currentView = CustomWebView1(
          key: UniqueKey(), dashBoardView1State: this, webUrl: webUrl);
    });
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
          key: scaffoldGlobalKey,
          body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          children: [
                            InkWell(
                                onTap: () {
                                  scaffoldGlobalKey.currentState.openDrawer();
                                  // getLastHistory();
                                  // Navigator.of(context).pop();
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(Constant.size15),
                                  child: Icon(FontAwesomeIcons.bars),
                                )),
                            Image.asset("assets/images/smarten-logo-sp.png",
                                fit: BoxFit.contain, width: Constant.size150, height: Constant.size36),
                            InkWell(
                              onTap: () {
                                _refresh();
                              },
                                child: Padding(
                                  padding: EdgeInsets.only(left: Constant.size150),
                                  child: Icon(FontAwesomeIcons.syncAlt),
                                ),
                            ),
                            SizedBox(
                                width: Constant.size6
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: Constant.size6,
                    color: AppColors.hintTextColor,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                            child: Icon(FontAwesomeIcons.longArrowAltLeft),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text(widget.strDashboardTitle,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: FontSize.s20,
                                  color: AppColors.BLACKCOLOR,
                                  fontFamily: 'Open-Sans',
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: Constant.size6,
                    color: AppColors.hintTextColor,
                  ),
                  Expanded(
                      child: widget.currentView != null
                          ? widget.currentView
                          : Center(
                        child: CircularProgressIndicator(),
                      )
                  ),
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  _refresh() {
    setState(() {
      widget.currentView = CustomWebView1(
          key: UniqueKey(), dashBoardView1State: this, webUrl: webUrl);
    });
  }
}