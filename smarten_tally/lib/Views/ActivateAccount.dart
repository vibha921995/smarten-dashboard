import 'dart:convert';
import 'dart:io';

import 'package:smarten_tally/Contants/AppColors.dart';
import 'package:smarten_tally/Contants/ConstantString.dart';
import 'package:smarten_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:smarten_tally/Helper/PreferenceHelper.dart';
import 'package:smarten_tally/Models/ErrorResponse.dart';
import 'package:smarten_tally/Models/LoginResponse.dart';
import 'package:smarten_tally/Utils/ApiUtils.dart';
import 'package:smarten_tally/Utils/screen_util.dart';
import 'package:smarten_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:http/http.dart' as http;
// import 'package:intl/intl.dart';
// import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/gestures.dart';
import 'package:smarten_tally/Views/SignInView.dart';

class ActivateAccount extends StatefulWidget {
  ActivateAccount({Key key}) : super(key: key);

  @override
  _ActivateAccountState createState() => _ActivateAccountState();
}

class _ActivateAccountState extends State<ActivateAccount> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  LoginResponse studentResponse;

  // Activate Account studentResponse;
  TextEditingController _etSmartenURL = new TextEditingController();
  TextEditingController _etUsername = new TextEditingController();
  TextEditingController _etPassword = new TextEditingController();
  FocusNode fnsmartenURL = new FocusNode();
  FocusNode fnusername = new FocusNode();
  FocusNode fnpassword = new FocusNode();
  bool isError = false;
  bool checkedValue = false;
  String errorText = "";

  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // String _homeScreenText = "Waiting for token...";
  // String _messageText = "Waiting for message...";
  // String fcmToken = "";
  //
  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();

    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   fcmToken = token;
    //   print("fcmToken===  $fcmToken");
    // });
  }

  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    print("onMessage: $message");

    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
    });
  }

  _checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet result  = " + result.toString());
        setState(() {
          isLoading = true;
        });
        // doLogin();
      }
    } on SocketException catch (e) {
      e.toString();
      setState(() {
        isLoading = false;
      });
//      if (!isDialogShowing) {
//        _showDialog();
//      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
//                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    textColor: AppColors.BLACKCOLOR,
                    height: 60.0,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.left,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: Constant.size50,
                              left: Constant.size2,
                              right: Constant.size2),
                          child: Icon(
                            FontAwesomeIcons.longArrowAltLeft,
                            color: AppColors.AppBlue,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: Constant.size50),
                          child: Text(
                            "Register",
                            style: TextStyle(
                                fontSize: FontSize.s25,
                                color: AppColors.BLACKCOLOR,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.w700
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Smarten URL",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s18,
                            color: AppColors.AppBlue,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: TextFormField(
                      style: TextStyle(
                          color: AppColors.BLACKCOLOR,
                          fontSize: FontSize.s16,
                          fontFamily: 'Open-Sans'),
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: 'Your Smarten URL',
                      ),
                      controller: _etSmartenURL,
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(fnusername);
                      },
                      focusNode: fnsmartenURL,
                      textInputAction: TextInputAction.next,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Username",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s18,
                            color: AppColors.AppBlue,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: TextFormField(
                      style: TextStyle(
                          color: AppColors.BLACKCOLOR,
                          fontSize: FontSize.s16,
                          fontFamily: 'Open-Sans'),
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: 'Your Smarten Username',
                      ),
                      controller: _etUsername,
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(fnpassword);
                      },
                      focusNode: fnusername,
                      textInputAction: TextInputAction.next,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Password",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s18,
                            color: AppColors.AppBlue,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: TextFormField(
                      obscureText: true,
                      style: TextStyle(
                          color: AppColors.BLACKCOLOR,
                          fontSize: FontSize.s16,
                          fontFamily: 'Open-Sans'),
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: 'Your Smarten Password',
                      ),
                      controller: _etPassword,
                      focusNode: fnpassword,
                      textInputAction: TextInputAction.done,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  SizedBox(
                    height: Constant.size10,
                  ),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size30,
                            left: Constant.size15),
                      ),
                      Checkbox(
                        value: checkedValue,
                        checkColor: AppColors.AppBlue,
                        activeColor: AppColors.AppBlue,
                        onChanged: (newValue) {
                          setState(() {
                            checkedValue = newValue;
                              // changeOverLapState(isOverlap);
                          });
                        },
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: Constant.size15,
                              left: Constant.size0_5,
                              right: Constant.size30),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: AppColors.BLACKCOLOR),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'I have read and agreed to the ',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Open-Sans',
                                        fontSize: FontSize.s16)),
                                TextSpan(
                                    text:
                                    String.fromCharCodes(new Runes(
                                        'Terms of Smarten EULA')),
                                    style: TextStyle(
                                        color: AppColors.AppBlue,
                                        fontFamily: 'Open-Sans',
                                        fontSize: FontSize.s16,
                                        fontWeight: FontWeight.w700,
                                        decoration: TextDecoration.underline,
                                        decorationColor: AppColors.AppBlue),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        // _launchInWebViewWithJavaScript(
                                        //     ConstantString.terms_url);
                                        print('Terms of Smarten EULA');
                                      }),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      if (_etSmartenURL.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState.showSnackBar(
                              ErrorSnakbar(
                                  'Please enter smarten url'));
                          fnsmartenURL.requestFocus();
                        });
                      } else if (_etUsername.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState.showSnackBar(
                              ErrorSnakbar(
                                  'Please enter username'));
                          fnusername.requestFocus();
                        });
                      } else if (_etPassword.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState
                              .showSnackBar(
                              ErrorSnakbar('Please enter password'));
                          fnpassword.requestFocus();
                        });
                      } else if (_etPassword.text.length < 8 ||
                          _etPassword.text.length == 15) {
                        scaffoldGlobalKey.currentState.showSnackBar(
                            ErrorSnakbar(
                                'Password must contain atleast 8 characters and maximum 15 characters'));
                        fnpassword.requestFocus();
                      } else if (checkedValue!=true) {
                        setState(() {
                          scaffoldGlobalKey.currentState
                              .showSnackBar(ErrorSnakbar('Please accept Term of Services and Privacy Policy'));
                        });
                      } else {
                        // doLogin();
                        //  _checkInternetConnection();
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: Constant.size30,
                          left: Constant.size30,
                          right: Constant.size30),
                      padding: EdgeInsets.all(Constant.size12),
                      decoration: BoxDecoration(
                        color: AppColors.AppBlue,
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            child: Expanded(
                              child: Text(
                                "Activate",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: FontSize.s20,
                                    fontFamily: 'fontawesome',
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size72,
                        bottom: Constant.size15),
                    child: Row(
                      // mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        InkWell(
                          onTap: (){},
                          child: Text(
                            "Already have an account?",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                color: AppColors.hintTextColor,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        SizedBox(
                          width: Constant.size4,
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "Sign In",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: AppColors.AppBlue,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w700,
                                decoration: TextDecoration.underline,
                                decorationColor: AppColors.AppBlue),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Don't know details?",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s14,
                            color: AppColors.hintTextColor,
                            fontFamily: 'fontawesome',
                            fontWeight: FontWeight.w600)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size4,
                        right: Constant.size4,
                    bottom: Constant.size20),
                    child: Text("Contact Smarten administrator in your organisation",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s14,
                            color: AppColors.hintTextColor,
                            fontFamily: 'fontawesome',
                            fontWeight: FontWeight.w600)),
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  // Future<void> _launchInWebViewWithJavaScript(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(
  //       url,
  //       // forceSafariVC: true,
  //       // forceWebView: true,
  //       enableJavaScript: true,
  //     );
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }
}