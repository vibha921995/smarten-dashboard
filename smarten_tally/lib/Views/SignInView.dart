import 'dart:convert';
import 'dart:io';

import 'package:smarten_tally/Contants/AppColors.dart';
import 'package:smarten_tally/Contants/ConstantString.dart';
import 'package:smarten_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:smarten_tally/Helper/PreferenceHelper.dart';
import 'package:smarten_tally/Models/ErrorResponse.dart';
import 'package:smarten_tally/Models/LoginResponse.dart';
import 'package:smarten_tally/Utils/ApiUtils.dart';
import 'package:smarten_tally/Utils/screen_util.dart';
import 'package:smarten_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:http/http.dart' as http;
// import 'package:intl/intl.dart';
// import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:smarten_tally/Views/ActivateAccount.dart';

class SignInView extends StatefulWidget {
  SignInView({Key key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  LoginResponse studentResponse;

  // LoginResponse studentResponse;
  TextEditingController _etUsername = new TextEditingController();
  TextEditingController _etPassword = new TextEditingController();
  FocusNode fnusername = new FocusNode();
  FocusNode fnpassword = new FocusNode();
  bool isError = false;
  String errorText = "";
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // String _homeScreenText = "Waiting for token...";
  // String _messageText = "Waiting for message...";
  // String fcmToken = "";
  //
  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();

    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   fcmToken = token;
    //   print("fcmToken===  $fcmToken");
    // });
  }

  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    print("onMessage: $message");

    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
    });
  }

  _checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet result  = " + result.toString());
        setState(() {
          isLoading = true;
        });
        // doLogin();
      }
    } on SocketException catch (e) {
      e.toString();
      setState(() {
        isLoading = false;
      });
//      if (!isDialogShowing) {
//        _showDialog();
//      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
//                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(padding: EdgeInsets.only(
                    top: Constant.size80
                  )),
                  Image.asset("assets/images/smarten-logo-sp.png",
                      fit: BoxFit.contain, width: Constant.size250, height: Constant.size50),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size50,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Sign In",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s25,
                            color: AppColors.BLACKCOLOR,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Username",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s18,
                            color: AppColors.AppBlue,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: TextFormField(
                      style: TextStyle(
                          color: AppColors.BLACKCOLOR,
                          fontSize: FontSize.s16,
                          fontFamily: 'Open-Sans'),
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: 'Your Smarten Username',
                      ),
                      controller: _etUsername,
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(fnpassword);
                      },
                      focusNode: fnusername,
                      textInputAction: TextInputAction.next,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text("Password",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s18,
                            color: AppColors.AppBlue,
                            fontFamily: 'Open-Sans',
                          fontWeight: FontWeight.w700)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: TextFormField(
                      obscureText: true,
                      style: TextStyle(
                          color: AppColors.BLACKCOLOR,
                          fontSize: FontSize.s16,
                          fontFamily: 'Open-Sans'),
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: 'Your Smarten Password',
                      ),
                      controller: _etPassword,
                      focusNode: fnpassword,
                      textInputAction: TextInputAction.done,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (_etUsername.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState.showSnackBar(
                              ErrorSnakbar(
                                  'Please enter username'));
                          fnusername.requestFocus();
                        });
                      } else if (_etPassword.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState
                              .showSnackBar(ErrorSnakbar('Please enter password'));
                          fnpassword.requestFocus();
                        });
                      } else if (_etPassword.text.length < 8 || _etPassword.text.length == 15) {
                        scaffoldGlobalKey.currentState.showSnackBar(
                            ErrorSnakbar('Password must contain atleast 8 characters and maximum 15 characters'));
                        fnpassword.requestFocus();
                      } else {
                        // doLogin();
                        //  _checkInternetConnection();
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DashBoardView()));
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: Constant.size30,
                          left: Constant.size30,
                          right: Constant.size30),
                      padding: EdgeInsets.all(Constant.size12),
                      decoration: BoxDecoration(
                        color: AppColors.AppBlue,
                          borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            child: Expanded(
                              child: Text(
                                "Sign In",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: FontSize.s20,
                                    fontFamily: 'fontawesome',
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: Constant.size120,
                        right: Constant.size120,
                        top: Constant.size30),
                    child: new Container(
                        margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Divider(
                          color: AppColors.BLACKCOLOR,
                          height: 36,
                        )),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ActivateAccount()));
                          },
                          child: Text(
                            "Not Registered?",
                              textAlign: TextAlign.center,
                            style: TextStyle(
                                color: AppColors.AppBlue,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w700,
                                decoration: TextDecoration.underline,
                                decorationColor: AppColors.AppBlue),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  // Future<void> _launchInWebViewWithJavaScript(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(
  //       url,
  //       // forceSafariVC: true,
  //       // forceWebView: true,
  //       enableJavaScript: true,
  //     );
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s16,
                  fontFamily: 'Open-Sans'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

//   Future doLogin() async {
//     setState(() {
//       isLoading = true;
//     });
//     try {
//       PackageInfo packageInfo = await PackageInfo.fromPlatform();
//       var body = {
//         "email_phone": _etUsername.text.trim(),
//         "password": _etPassword.text.trim(),
//          "login_with": "M",
//         "appversion": packageInfo.version,
//         "firebase": fcmToken,
//         "app_version": "3.0",
//         // "ostype": Platform.isAndroid?"Android":"iOS"
//       };
//       var response = await http.post(Uri.encodeFull(ApiUtils.LOGIN_URL), body: body);
//       print("Login response : ----" + response.body);
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
//             response.body
//                 .replaceFirst("associatedcompany", '"associatedcompany"')));
//         setState(() {
//           isLoading = false;
//         });
//         if (errorResponse.success == "1") {
//           studentResponse = new LoginResponse.fromJson(json.decode(response.body
//               .replaceFirst("associatedcompany", '"associatedcompany"')));
//           scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//               content: Text(studentResponse.message),
//               backgroundColor: Colors.green));
//           preferenceHelper.saveUserData(studentResponse.response);
//           preferenceHelper.saveIsUserLoggedIn(true);
//           var now = new DateTime.now();
//           var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
//           String formattedDate = formatter.format(now);
//           print(formattedDate);
//           DateTime dateTimeCreatedAt = DateTime.parse(studentResponse.response.subscriptionEndDate.toString());
//           DateTime dateTimeNow = DateTime.now();
//           final differenceInDays = dateTimeCreatedAt.difference(dateTimeNow).inDays;
//           print(differenceInDays);
//           //if (differenceInDays > 0) {
// //            Navigator.pushReplacement(
// //                context,
// //                MaterialPageRoute(
// //                    builder: (BuildContext context) => IAP()));
// //          }
// //          else {
//            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DashBoardView()));
// //          }
//         } else {
//           scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(errorResponse.message));
//         }
//       } else if (response.statusCode == 400) {
//         setState(() {
//           isLoading = false;
//         });
//         scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
//       } else {
//         setState(() {
//           isLoading = false;
//         });
//         scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
//       }
//     } on SocketException catch (e) {
//       print(e.message);
//       setState(() {
//         isLoading = false;
//       });
//       scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.network_error));
//     } on Exception catch (e) {
//       print(e.toString());
//       setState(() {
//         isLoading = false;
//       });
//       scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
//     }
//   }
}

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }
}
