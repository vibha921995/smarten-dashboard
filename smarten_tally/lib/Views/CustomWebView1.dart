import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// import 'package:flutter_downloader/flutter_downloader.dart';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:smarten_tally/Contants/AppColors.dart';
import 'package:smarten_tally/Utils/screen_util.dart';

import 'package:smarten_tally/Views/DashboardView1.dart';


class CustomWebView1 extends StatefulWidget {
  final DashboardView1State dashBoardView1State;
  final String webUrl;

  // CustomWebView(this.dashBoardView1State,{Key key});
  CustomWebView1({Key key, this.dashBoardView1State, this.webUrl})
      : super(key: key);

  @override
  _CustomWebView1State createState() => _CustomWebView1State();
}

class _CustomWebView1State extends State<CustomWebView1> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isWebLoading = false;
  InAppWebViewController _webViewController;
  String _localPath;
  static var httpClient = new HttpClient();
  String progress = '0';
  String loadingText = "Downloading file....Please wait";
  ReceivePort _port = ReceivePort();
  Directory _downloadsDirectory;

  @override
  void initState() {
    super.initState();
    main();
  }

  Future main() async {
    // await Permission.storage.request();
    // initFlutterDownloader();
    // _bindBackgroundIsolate();

    print("Web loading url=="+widget.webUrl);
    if(Platform.isIOS) {
      Directory dir = (await getApplicationDocumentsDirectory());
      _localPath = dir.path;
    }else{
      _localPath = (await DownloadsPathProvider.downloadsDirectory).path;
      final savedDir = Directory(_localPath);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        savedDir.create();
      }
    }
  }

  // Future<void> initFlutterDownloader() async {
  //   WidgetsFlutterBinding.ensureInitialized();
  //   try {
  //     await FlutterDownloader.initialize(
  //       debug: true, // optional: set false to disable printing logs to console
  //     );
  //   } catch (Ex) {}
  //   FlutterDownloader.registerCallback(downloadCallback);
  // }

  // void _bindBackgroundIsolate() {
  //   bool isSuccess = IsolateNameServer.registerPortWithName(
  //       _port.sendPort, 'downloader_send_port');
  //   if (!isSuccess) {
  //     _unbindBackgroundIsolate();
  //     _bindBackgroundIsolate();
  //     return;
  //   }
  //
  //   _port.listen((dynamic data) {
  //     String id = data[0];
  //     DownloadTaskStatus status = data[1];
  //     int progress = data[2];
  //     print("Data listen ===" + progress.toString());
  //     if (status == DownloadTaskStatus.complete) {
  //       print('Call download completed');
  //       setState(() {
  //         isWebLoading = false;
  //       });
  //       Navigator.of(context).pop();
  //       _showFinishDialog(id);
  //     }
  //   });
  // }

  // void _unbindBackgroundIsolate() {
  //   IsolateNameServer.removePortNameMapping('downloader_send_port');
  // }

  // @override
  // void dispose() {
  //   _unbindBackgroundIsolate();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     // resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(children: [
        InAppWebView(
          initialUrl: widget.webUrl,
          initialOptions: InAppWebViewGroupOptions(
            crossPlatform: InAppWebViewOptions(
              debuggingEnabled: true,
              useOnDownloadStart: true,
              javaScriptEnabled: true,
              verticalScrollBarEnabled: true,
              useShouldOverrideUrlLoading: true
            ),
          ),
          onWebViewCreated: (InAppWebViewController controller) {
            _webViewController = controller;
          },

          shouldOverrideUrlLoading: (controller, request) async {
            var url = request.url;
            var uri = Uri.parse(url);
            print("shouldOverrideUrlLoading.. $url");

            // if(Platform.isIOS)
            // {
              if (uri.toString().contains("export.php"))
              {
                print("onDownloadStart====iOS.. $url");
                _showMyDialog();
//                String savePath = await getFilePath("tallyFile_"+DateTime.now().millisecondsSinceEpoch.toString());
//                download2(url, savePath,true);
//                 final taskId = await FlutterDownloader.enqueue(
//                     url: url,
//                     // savedDir: (await getApplicationDocumentsDirectory()).path,
//                     savedDir: _localPath,
//                     showNotification: true,
//                     // show download progress in status bar (for Android)
//                     openFileFromNotification: true);
//
             //   downloadFile(uri.toString(), "Download_tallyFile_"+DateTime.now().millisecondsSinceEpoch.toString());

              }
            // }
            return ShouldOverrideUrlLoadingAction.ALLOW;
          },
          onConsoleMessage: (controller, ConsoleMessage consoleMessage) {
            print("Webview console message======" + consoleMessage.message);
            if (consoleMessage.message.startsWith("DoShareUrl")) {
              print("share call...");

            //  List<String> urls = consoleMessage.message.split(":");
              //shareLink(urls[1]);
            }
          },

          onDownloadStart: (controller, url) async {
            print("onDownloadStart==== $url");

          },
          onProgressChanged: (InAppWebViewController controller, int progress) {
            setState(() {
              if (progress == 100) {
                isWebLoading = false;
              } else {
                isWebLoading = true;
              }
              print("progress===" + progress.toString());
            });
          },
        ),
        // ProgressIndicatorLoader(AppColors.primary, isWebLoading)

        isWebLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : Container()
      ]),
    );
  }

  // Future<File> _downloadFile(String url, String filename) async {
  //   var request = await httpClient.getUrl(Uri.parse(url));
  //   var response = await request.close();
  //   var bytes = await consolidateHttpClientResponseBytes(response);
  //   String dir = (await getApplicationDocumentsDirectory()).path;
  //   File file = new File('$dir/$filename');
  //   await file.writeAsBytes(bytes);
  //   return file;
  // }


  Future<String> getFilePath(uniqueFileName) async {
    String path = '';
//    Directory dir = _findLocalPath());
    path = _localPath + Platform.pathSeparator +'$uniqueFileName.pdf';
    return path;
  }

  String capitalize(String string) {
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }


  // static void downloadCallback(
  //   String id, DownloadTaskStatus status, int progress) {
  //   print('Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
  //   final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port');
  //   send.send([id, status, progress]);
  // }

  Future<void> _showFinishDialog(String taskId) async {
    String saveMessage = "";
    if(Platform.isIOS)
      saveMessage = "File downloaded successfully on you phone under Files -> SmartenApps For Tally folder";
    else
      saveMessage = "File downloaded successfully on your phone under Downloads folder";

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Downloaded Successfully',
              style: TextStyle(
                  color: AppColors.greenButtonBorder, fontSize: FontSize.s15)),
          content: WillPopScope(
            onWillPop: () {},
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(saveMessage),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size4),
                child: Text('OK',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size4),
                child: Text('OPEN',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
                // FlutterDownloader.open(taskId: taskId);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: WillPopScope(
            onWillPop: () {},
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(loadingText),
                ],
              ),
            ),
          ),
        );
      },
    );
  }



  // static shareFile() {
  //   print("TAG in shareFile==========");
  //
  //   FlutterShare.shareFile(
  //     title: 'Example share',
  //     text: 'Example share text',
  //     filePath: "tellyfile.pdf"
  //   );
  //
  //   // Share.shareFile(File('$documentDirectory/flutter.png'),
  //   //     subject: 'URL File Share',
  //   //     text: 'Hello, check your share files!',
  //   //     sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  // }

  // Future<Io.File> getImageFromNetwork(String url) async {
  //   var cacheManager = await CacheManager.getInstance();
  //   Io.File file = await cacheManager.getFile(url);
  //   return file;
  // }

  static Future<void> shareFile() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/tallyfileshare.pdf');

    await FlutterShare.shareFile(
        title: 'Share file', text: 'Example share text', filePath: file.path);

    // await Share.shareFiles([file.path],
    //     text: 'Share file',
    //     subject: 'Share file',
    //     sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
          ),
          SizedBox(

            width: Constant.size8,
          ),
          Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: FontSize.s15,
                fontFamily: 'fontawesome'),
          ),
        ],
      ),
    );
  }
}