import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:smarten_tally/Contants/AppColors.dart';
import 'package:smarten_tally/Contants/ConstantString.dart';
import 'package:smarten_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:smarten_tally/Helper/PreferenceHelper.dart';
// import 'package:smarten_tally/Models/CompanyListResponse.dart';
// import 'package:smarten_tally/Models/CompanyModel.dart';
import 'package:smarten_tally/Models/LogResponse.dart';
import 'package:smarten_tally/Models/MenuModel.dart';
import 'package:smarten_tally/Utils/ApiUtils.dart';
import 'package:smarten_tally/Utils/screen_util.dart';
import 'package:smarten_tally/Views/DashboardView1.dart';
// import 'package:smarten_tally/Views/ComplanyList.dart';
// import 'package:smarten_tally/Views/HowWorks.dart';
// import 'package:smarten_tally/Views/InviteFriends.dart';
import 'package:smarten_tally/Views/SignInView.dart';
// import 'package:smarten_tally/Views/SmartenApps.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smarten_tally/Views/DashBoardView.dart';
// import 'package:smarten_tally/Views/Payment.dart';
// import 'package:smarten_tally/Views/FaqView.dart';
// import 'package:smarten_tally/Views/IAP.dart';
// import 'IAP_New.dart';
import 'package:http/http.dart' as http;

// import 'AboutUs.dart';
import 'CustomWebView1.dart';
// import 'Support.dart';
bool isPaymentFromIAP = false;

class BrowseView extends StatefulWidget {

  final DashBoardViewState dashBoardViewState;

  Widget currentView;
  bool isWebLoading = false;

  // LogsView(this.dashBoardViewState,{Key key});
  BrowseView({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _BrowseViewState createState() => _BrowseViewState();
}

class _BrowseViewState extends State<BrowseView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  bool isLoading = false;
  bool isDesktopVerification = false;
  bool isPayment = false;

  var _textController = TextEditingController();

  // final flutterWebviewPlugin = new FlutterWebviewPlugin();

  // WebViewController webViewController;
  List<MenuModel> MenuList = [];
  String fullName = "", emailID = "", currentDate = "", webUrl = "";
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  static String client_Id = "";
  static String user_Id = "";
  static String lastAttemp = "";
  static String subscription_endDate = "";

  List<LogModel> LogsList = [];
  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // List<CompanyModel> companyList = [];
  // NotificationDetails platformChannelSpecifics;
  static InAppWebViewController webViewController;
  int exitCounter = 0;

  String _homeScreenText = "Waiting for token...";
  String _messageText = "Waiting for message...";

  /// Initialize the [FlutterLocalNotificationsPlugin] package.
  // FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  //     FlutterLocalNotificationsPlugin();

  TextEditingController _etSearch = new TextEditingController();
  FocusNode fnsearch = new FocusNode();

  @override
  void initState() {
    super.initState();

    setMenuList();
    getCurrentDate();
    getSharedPreferenceObject();
  }

  Future<void> getSharedPreferenceObject() async {
    setState(() {
      widget.isWebLoading = true;
    });
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      setState(() {
        fullName = "First + Last Name";//preferenceHelper.getUserData().fullName;
        emailID = "emailaddress@email.com";//preferenceHelper.getUserData().companyName;
        client_Id = ""; //preferenceHelper.getUserData().clientId;
        user_Id = ""; //preferenceHelper.getUserData().userId;

        subscription_endDate = preferenceHelper.getUserData().subscriptionEndDate.toString();
        isDesktopVerification =
        preferenceHelper.getUserData().tallyScheduling != "0"
            ? true
            : false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        drawer: Drawer(
            elevation: 0,
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(padding:
                  EdgeInsets.only(top: Constant.size30,left: Constant.size30,right: Constant.size30)
                  ),
                  Image.asset("assets/images/smarten-logo-sp.png",
                      fit: BoxFit.contain, width: Constant.size200, height: Constant.size40),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size30,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text(fullName,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s16,
                            color: AppColors.BLACKCOLOR,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w600)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size0_5,
                        left: Constant.size30,
                        right: Constant.size30),
                    child: Text(emailID,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: FontSize.s14,
                            color: AppColors.hintTextColor,
                            fontFamily: 'Open-Sans')),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size60,
                        left: Constant.size30,
                        right: Constant.size30),
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    addSemanticIndexes: true,
                    itemCount: MenuList.length,
                    itemBuilder: (context, index) {
                      return menuItemView(/*promoList[index], */ index);
                    },
                    shrinkWrap: true,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: Constant.size150,
                        bottom: Constant.size30),
                    color: AppColors.AppPinkShade2,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: Constant.size12, bottom: Constant.size12),
                      child: InkWell(
                        onTap: () {
                          preferenceHelper.clearAll();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignInView()));
                        },
                        child: Padding(
                          padding:
                          EdgeInsets.only(top: Constant.size3,
                              left: Constant.size25,
                              bottom: Constant.size3),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                FontAwesomeIcons.signOutAlt,
                                color: AppColors.BLACKCOLOR,
                              ),
                              SizedBox(
                                width: Constant.size8,
                              ),
                              Text("Sign Out",
                                  style: TextStyle(
                                      color: AppColors.BLACKCOLOR,
                                      fontFamily: 'Open-Sans',
                                      fontSize: FontSize.s16,
                                      fontWeight: FontWeight.w600))
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ) // Populate the Drawer in the next step.
        ),
        //  resizeToAvoidBottomPadding: true,
        key: scaffoldGlobalKey,
        body: WillPopScope(
          onWillPop: () async {
            // Earlier working function of backbutton
            // print("in WillPopScope exitCounter=="+exitCounter.toString());
            // if (MenuList[0].isSelected)
            // {
            //   if (webViewController != null) {
            //     if (await webViewController.canGoBack()) {
            //       exitCounter = 0;
            //       webViewController.goBack();
            //     } else {
            //       if (exitCounter == 0) {
            //         scaffoldGlobalKey.currentState.showSnackBar(new SnackBar(
            //           backgroundColor: Colors.black,
            //           content: Row(
            //             crossAxisAlignment: CrossAxisAlignment.center,
            //             mainAxisSize: MainAxisSize.max,
            //             children: [
            //               Icon(
            //                 FontAwesomeIcons.exclamationTriangle,
            //                 size: Constant.size15,
            //                 color: Colors.white,
            //               ),
            //               SizedBox(
            //                 width: Constant.size8,
            //               ),
            //               Expanded(
            //                 child: Text(
            //                   "Press again to close App",
            //                   style: TextStyle(
            //                       color: Colors.white,
            //                       fontSize: FontSize.s15,
            //                       fontFamily: 'fontawesome'),
            //                   overflow: TextOverflow.ellipsis,
            //                   maxLines: 10,
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ));
            //         exitCounter++;
            //       } else {
            //         print("in if condition   0");
            //         SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            //       }
            //     }
            //   } else {
            //     _showExitDialog();
            //   }
            // } else {
            //   _showExitDialog();
            // }
            _showExitDialog();
            return;
          },
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                color: AppColors.progressPathShadowColor,
                child: ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    Stack(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              InkWell(
                                  onTap: () {
                                    scaffoldGlobalKey.currentState.openDrawer();
                                    // getLastHistory();
                                    // Navigator.of(context).pop();
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.all(Constant.size15),
                                    child: Icon(FontAwesomeIcons.bars),
                                  )),
                              Image.asset("assets/images/smarten-logo-sp.png",
                                  fit: BoxFit.contain, width: Constant.size150, height: Constant.size36)
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: Constant.size6,
                      color: AppColors.hintTextColor,
                    ),
                    Row(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            // color: MenuList[index].isSelected
                            //     ? AppColors.AppPinkShade2
                            //     : Colors.transparent,
                            child: Padding(
                              padding:
                              EdgeInsets.only(top: Constant.size50,
                                  left: Constant.size10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(Constant.size14),
                                    child: Text("Recent",
                                        style: TextStyle(
                                            shadows: [
                                              Shadow(
                                                  color: AppColors.BLACKCOLOR,
                                                  offset: Offset(0, -20))
                                            ],
                                            color: Colors.transparent,
                                            fontFamily: 'fontawesome',
                                            fontSize: FontSize.s16,
                                            fontWeight: FontWeight.w600,
                                            decoration: TextDecoration.underline,
                                            decorationColor: AppColors.BLACKCOLOR,
                                            decorationThickness: 0.5)),
                                  ),
                                  Divider(
                                    color: AppColors.BLACKCOLOR,
                                    height: 36,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // onManutap(MenuList[index].name, index);
                          },
                          child: Container(
                            // color: MenuList[index].isSelected
                            //     ? AppColors.AppPinkShade2
                            //     : Colors.transparent,
                            child: Padding(
                              padding:
                              EdgeInsets.only(top: Constant.size50,
                                  left: Constant.size20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(Constant.size14),
                                    child: Text("My Favourites",
                                        style: TextStyle(
                                          shadows: [
                                            Shadow(
                                                color: AppColors.BLACKCOLOR,
                                                offset: Offset(0, -20))
                                          ],
                                          color: Colors.transparent,
                                          fontFamily: 'fontawesome',
                                          fontSize: FontSize.s16,
                                          fontWeight: FontWeight.w600,
                                          decoration: TextDecoration.underline,
                                          decorationColor: AppColors.BLACKCOLOR,
                                          decorationThickness: 0.5)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        BrowseView()));
                          },
                          child: Container(
                            // color: MenuList[index].isSelected
                            //     ? AppColors.AppPinkShade2
                            //     : Colors.transparent,
                            child: Padding(
                              padding:
                              EdgeInsets.only(top: Constant.size50,
                                  left: Constant.size20,
                                  right: Constant.size20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  SizedBox(
                                    width: Constant.size3,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(Constant.size14),
                                    child: Text("Browse",
                                        style: TextStyle(
                                            shadows: [
                                              Shadow(
                                                  color: AppColors.BLACKCOLOR,
                                                  offset: Offset(0, -20))
                                            ],
                                            color: Colors.transparent,
                                            fontFamily: 'fontawesome',
                                            fontSize: FontSize.s16,
                                            fontWeight: FontWeight.w600,
                                            decoration: TextDecoration.underline,
                                            decorationColor: AppColors.BLACKCOLOR,
                                            decorationThickness: 2.85)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: Constant.size0_5,
                          left: Constant.size25,
                          right: Constant.size25),
                      child: new TextField(controller: _textController,
                        style: TextStyle(
                            color: AppColors.BLACKCOLOR,
                            fontSize: FontSize.s16,
                            fontFamily: 'Open-Sans',
                            fontWeight: FontWeight.w600),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'search...',
                          suffixIcon: IconButton(
                              icon: Icon(Icons.clear,
                                color: AppColors.BLACKCOLOR,),
                              onPressed: () {
                                this.setState(() {
                                  _textController.clear();
                                });
                              }
                          ),
                        ),
                        focusNode: fnsearch,
                        textInputAction: TextInputAction.done,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: Constant.size25,
                          right: Constant.size25
                      ),
                      child: new Container(
                          child: Divider(
                            color: AppColors.hintTextColor,
                            thickness: Constant.size4,
                            height: 0,
                          )),
                    ),
                    InkWell(
                      onTap: () {
                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (BuildContext context) => DashboardView1(strDashboardTitle: "Dashboard 1")));
                      },
                      child: Container(
                        child:Padding(padding: EdgeInsets.only(
                            top: Constant.size20,
                            left: Constant.size20,
                            bottom: Constant.size20),
                          child: Row(
                            children: [
                              SizedBox(
                                width: Constant.size3,
                              ),
                              Icon(
                                FontAwesomeIcons.solidFolder,
                                size: Constant.size20,
                                color: AppColors.BLACKCOLOR,
                              ),
                              SizedBox(
                                width: Constant.size3,
                              ),
                              Padding(
                                padding: EdgeInsets.all(Constant.size10),
                                child: Text(
                                  "Sales Demo",
                                  style: TextStyle(
                                      color: AppColors.BLACKCOLOR,
                                      fontSize: FontSize.s16,
                                      fontFamily: 'Open-Sans',
                                      fontWeight: FontWeight.w600
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: Constant.size0_5,
                          bottom: Constant.size0_5),
                      child: new Container(
                          child: Divider(
                            color: AppColors.BLACKCOLOR,
                            height: 0,
                          )),
                    ),
                    InkWell(
                      onTap: () {
                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (BuildContext context) => DashboardView1(strDashboardTitle: "Dashboard 1")));
                      },
                      child: Container(
                        child:Padding(padding: EdgeInsets.only(
                            top: Constant.size20,
                            left: Constant.size20,
                            bottom: Constant.size20),
                          child: Row(
                            children: [
                              SizedBox(
                                width: Constant.size3,
                              ),
                              Icon(
                                FontAwesomeIcons.solidFolder,
                                size: Constant.size20,
                                color: AppColors.BLACKCOLOR,
                              ),
                              SizedBox(
                                width: Constant.size3,
                              ),
                              Padding(
                                padding: EdgeInsets.all(Constant.size10),
                                child: Text(
                                  "Demo App",
                                  style: TextStyle(
                                      color: AppColors.BLACKCOLOR,
                                      fontSize: FontSize.s16,
                                      fontFamily: 'Open-Sans',
                                      fontWeight: FontWeight.w600
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ProgressIndicatorLoader(AppColors.primary, isLoading)
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showExitDialog() async {
    String saveMessage = "";
    saveMessage = "Back button is disabled on SmartenApps. You can use navigation options available within mobile app.";
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert',
              style: TextStyle(
                  color: AppColors.greenButtonBorder, fontSize: FontSize.s15)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(saveMessage),
              ],
            ),
          ),
          actions: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size6),
                child: Text('OK',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget menuItemView(int index) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            onManutap(MenuList[index].name, index);
          },
          child: Container(
            color: MenuList[index].isSelected
                ? AppColors.AppPinkShade2
                : Colors.transparent,
            child: Padding(
              padding:
              EdgeInsets.only(top: Constant.size3,
                  left: Constant.size20,
                  bottom: Constant.size3),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: Constant.size3,
                  ),
                  Icon(
                    MenuList[index].iconData,
                    size: Constant.size20,
                    color: AppColors.lightGreyColor,
                  ),
                  SizedBox(
                    width: Constant.size3,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size14),
                    child: Text(MenuList[index].name,
                        style: TextStyle(
                            color: AppColors.BLACKCOLOR,
                            fontFamily: 'Open-Sans',
                            fontSize: FontSize.s16,
                            fontWeight: FontWeight.w600)),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  void setMenuList() {
    MenuList.add(new MenuModel("Home",
        FontAwesomeIcons.home, true, false));
    MenuList.add(new MenuModel("Browse",
        FontAwesomeIcons.solidWindowMaximize, true, true));
    MenuList.add(new MenuModel("Settings",
        FontAwesomeIcons.cog, true, false));
    MenuList.add(new MenuModel("About Us",
        FontAwesomeIcons.users, true, false));
  }

  void getCurrentDate() {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MMM-yyyy');
    setState(() {
      currentDate = formatter.format(now);
    });
  }

  void onManutap(String name, int pos) {
    if (MenuList[pos].isActive) // temp edit
        {
      setState(() {
        for (int i = 0; i < MenuList.length; i++) {
          MenuList[i].isSelected = false;
        }
        MenuList[pos].isSelected = true;
      });

      if (name.toLowerCase() == "browse") {
        Navigator.of(context).pop();
        setState(() {
        });
      }
      if (name.toLowerCase() == "dashboard" || name.toLowerCase() == "home") {
        if (scaffoldGlobalKey.currentState.isDrawerOpen)
          Navigator.of(context).pop();
        setState(() {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      DashBoardView()));
        });
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  _refresh() {
    String sel_name = "";
    for (int i = 0; i < MenuList.length; i++) {
      if (MenuList[i].isSelected && MenuList[i].isActive) {
        sel_name = MenuList[i].name;
        break;
      }
    }

    if (sel_name.toLowerCase() == "home") {
      setState(() {

      });
    } else if (sel_name.toLowerCase() == "browse") {
      setState(() {

      });
    }
  }
}
