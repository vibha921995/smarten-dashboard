import 'dart:ui';
import 'package:flutter/material.dart';

class BorderedTab extends StatelessWidget implements Tab {
  const BorderedTab({
    Key key,
    this.text,
    this.borderColor=Colors.grey,
    this.width=0.5,
  }) : super(key: key);

  final String text;
  final Color borderColor;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Container(
                child: Center(
                    child: Text(text)
                ),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: width,
                      color: borderColor,
                    ),
                  ),
                ),
              ),
            ),
          ]
      ),
    );
  }

  @override
  // TODO: implement child
  Widget get child => null;

  @override
  // TODO: implement icon
  Widget get icon => null;

  @override
  // TODO: implement iconMargin
  EdgeInsetsGeometry get iconMargin => throw UnimplementedError();
}
