import 'dart:async';

import 'package:flutter/material.dart';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/services.dart';
import 'Utils/screen_util.dart';
import 'Contants/AppColors.dart';
import 'Views/DashBoardView.dart';

import 'package:smarten_tally/Views/DashBoardView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Helper/PreferenceHelper.dart';
import 'Views/SignInView.dart';

// /// Define a top-level named handler which background/terminated messages will
// /// call.
// ///
// /// To verify things are working, check out the native platform logs.
// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   // If you're going to use other Firebase services in the background, such as Firestore,
//   // make sure you call `initializeApp` before using other Firebase services.
//   // await Firebase.initializeApp();
//   // print('Handling a background message ${message.messageId}');
// }

/// Create a [AndroidNotificationChannel] for heads up notifications
// const AndroidNotificationChannel channel = AndroidNotificationChannel(
//   'high_importance_channel', // id
//   'High Importance Notifications', // title
//   'This channel is used for important notifications.', // description
//   importance: Importance.high,
// );
//
// /// Initialize the [FlutterLocalNotificationsPlugin] package.
// final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
// FlutterLocalNotificationsPlugin();

// static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
// print("onMessage: $message");
//
// if (message.containsKey('data')) {
// // Handle data message
// final dynamic data = message['data'];
// }
//
// if (message.containsKey('notification')) {
// // Handle notification message
// final dynamic notification = message['notification'];
// }
//
// // Or do other work.
// }


Future<void> main() async {

  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  //
  //
  // FirebaseMessaging messaging = FirebaseMessaging.instance;
  // // Set the background messaging handler early on, as a named top-level function
  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  //
  // /// Create an Android Notification Channel.
  // ///
  // /// We use this channel in the `AndroidManifest.xml` file to override the
  // /// default FCM channel to enable heads up notifications.
  // await flutterLocalNotificationsPlugin
  //     .resolvePlatformSpecificImplementation<
  //     AndroidFlutterLocalNotificationsPlugin>()
  //     ?.createNotificationChannel(channel);
  //
  // /// Update the iOS foreground notification presentation options to allow
  // /// heads up notifications.
  // await FirebaseMessaging._instance.setForegroundNotificationPresentationOptions(
  //   alert: true,
  //   badge: true,
  //   sound: true,
  // );

  runApp(MyApp());
}



class MyApp extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent, // status bar color
      ),
    );
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => _buildTheme(brightness),
      themedWidgetBuilder: (context, theme) => MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: theme,
        home: SplashScreen(),
      ),
    );
  }

  ThemeData _buildTheme(Brightness brightness) {
    return brightness == Brightness.dark
        ? ThemeData.dark().copyWith(
      textTheme: ThemeData.dark().textTheme.apply(fontFamily: 'roboto'),
      primaryColor: AppColors.CARD_BG_COLOR,
      scaffoldBackgroundColor: AppColors.BLACK_BG,
//      accentColor: AppColors.DIVIDER_COLOR,
//      hintColor: AppColors.ACTION_BUTTON_DIVIDER_COLOR,
//      backgroundColor: AppColors.TITLE_TEXT_COLOR,
//      primaryColorDark: AppColors.NAVIGATION_DRAWER_ITEM_COLOR,
//      bottomAppBarColor: AppColors.BUTTON_TEXT_COLOR,
//      canvasColor: AppColors.DIVIDER_COLOR,
//      buttonColor: AppColors.TITLE_TEXT_COLOR,
//      cardColor: AppColors.CARD_BG_COLOR,
//      cursorColor: AppColors.DIVIDER_COLOR,
//      highlightColor: AppColors.CARD_LIGHT,
    )
        : ThemeData.light().copyWith(
      textTheme: ThemeData.light().textTheme.apply(fontFamily: 'roboto'),
      primaryColor: AppColors.CARD_BG_COLOR,
      scaffoldBackgroundColor: AppColors.WHITECOLOR,
//        accentColor: AppColors.WHITECOLOR,
//        hintColor: AppColors.LINECOLOR,
//        backgroundColor: AppColors.TEXTCOLOR,
//        primaryColorDark: AppColors.TEXTCOLOR,
//        bottomAppBarColor: AppColors.TEXTCOLOR,
//        canvasColor: AppColors.PRIMARYCOLOR,
//        buttonColor: AppColors.DAYICONCOLOR,
//        cardColor: AppColors.WHITECOLOR,
//        cursorColor: AppColors.LINECOLOR,
//        highlightColor: AppColors.PRIMARYCOLOR
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

// class _SplashScreenState extends State<SplashScreen> {
//
//   @override
//   void initState() {
//     super.initState();
//     SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//       statusBarColor: Colors.transparent,
//     ));
//     startTimeHome();
//   }

  // Future<void> getSharedPreferenceObject() async {
  //   SharedPreferences.getInstance().then((SharedPreferences sp) {
  //     prefs = sp;
  //     preferenceHelper = new PreferenceHelper(prefs);
  //     if (preferenceHelper.getIsUserLoggedIn()) {
  //       startTimeHome();
  //     } else {
  //       startTimeLogin();
  //     }
  //   });
  // }

class _SplashScreenState extends State<SplashScreen> {
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    getSharedPreferenceObject();

  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      if (preferenceHelper.getIsUserLoggedIn()) {
        startTimeHome();
      } else {
        startTimeLogin();
      }
    });
  }

  startTimeLogin() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, navigationLogin);
  }

  startTimeHome() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationHome);
  }

  void navigationLogin() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => SignInView()));
    // MaterialPageRoute(builder: (BuildContext context) => Verification()));
  }

  void navigationHome() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => DashBoardView()));    //temp comment
    // MaterialPageRoute(builder: (BuildContext context) => Verification()));
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/splash_screen.png"),
                fit: BoxFit.fill,
              )),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
