import 'package:flutter/material.dart';
import 'package:smarten_tally/Utils/screen_util.dart';
import 'AppColors.dart';

class ProgressIndicatorLoader extends StatefulWidget {
  final bool isLoading;
  final Color color;

  ProgressIndicatorLoader(this.color,this.isLoading);

  @override
  _ProgressIndicatorState createState() => _ProgressIndicatorState();
}

class _ProgressIndicatorState extends State<ProgressIndicatorLoader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: widget.isLoading
          ? new Container(
          color: AppColors.transparentblack,
          alignment: Alignment.center,
          child: new Center(
              child: SizedBox(
                height: Constant.size44,
                width: Constant.size44,
                child: new CircularProgressIndicator(
                  strokeWidth: Constant.size4,
                  valueColor: AlwaysStoppedAnimation<Color>(widget.color),
                  backgroundColor: Colors.transparent,
                ),
              )))
          : new Container(),
    );
  }
}
