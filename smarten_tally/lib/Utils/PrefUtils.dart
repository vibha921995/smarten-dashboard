class PrefUtils{
  static const String USER_DATA = "userData";
  static const String USER_DATA_ARRAY = "userDataArray";
  static const String IS_LOGGED_IN = "isLoggedIn";
  static const String IS_BALANCE_UPDATED = "isBalanceUpdated";
  static const String USER_BALANCE = "userBalance";
  static const String FB_TOKEN = "fbToken";
  static const String LOGIN_TYPE = "LoginType";
  static const String RECEIVE_ADDRESS = "receiveAddress";
  static const String IS_USERNEW = "IsUserNew";
  static const String APP_LANG = "AppLang";
  static const String USER_DATA_REGISTER = "userDataRegister";
  static const String SEL_COMPANY = "selectedCompany";

}