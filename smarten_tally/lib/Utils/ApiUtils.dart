

class ApiUtils {
  // static const String BASE_URL = "https://www.tallymobileapp.com/tallyapp/webservices/desktopapp/";
  static const String BASE_URL = "https://finance.smartenapps.com/webapi/";
  static const String SAP_CLIENT = "100";
  static const String APP_TOKEN_STRING = "14faa1e8fa9d5a219d0ae049809592d0bd65854faafe993faa1e8fa9d5";
  static const String ACCEPT = "Accept";
  static const String USERNAME = "username";
  static const String PASSWORD = "password";

  static const String HEADER_TYPE = "application/json";
  static const String LOGIN_URL = BASE_URL +"login.php";
  static const String FPASS_URL = BASE_URL +"login_varification.php";
  static const String LOGS_URL = BASE_URL +"extraction_logs.php";
  static const String COMPANYLIST_URL = BASE_URL +"user_mapping.php";
  static const String REGISTER_URL = BASE_URL +"register.php";
  static const String REGISTER_VERIFICATION_URL = BASE_URL +"register_verification.php";
  static const String PAYMENT_URL = BASE_URL +"payment.php";

}